Name:       	sharutils
Version:    	4.15.2
Release:    	17
Summary:    	The set of GNU shar utilities
License:    	GPL-3.0-or-later AND (GPL-3.0-or-later AND BSD-4-Clause) AND (LGPL-3.0-or-later OR BSD-3-Clause) AND LGPL-2.0-or-later AND LGPL-3.0-or-later AND Public Domain AND GFDL-1.3-or-later
URL:        	https://www.gnu.org/software/sharutils/
Source0:     	https://ftp.gnu.org/gnu/sharutils/%{name}-%{version}.tar.xz
Source1:	LicenseList

Patch0:     	%{name}-4.14.2-Pass-compilation-with-Werror-format-security.patch
Patch1:     	%{name}-4.15.2-Fix-a-heap-buffer-overflow-in-find_archive.patch
Patch2:     	%{name}-4.15.2-fflush-adjust-to-glibc-2.28-libio.h-removal.patch
Patch6000:      backport-Fix-building-with-GCC-10.patch
Patch6001:      backport-Do-not-include-lib-md5.c-into-src-shar.c.patch

BuildRequires:  gcc make
BuildRequires:	gettext
BuildRequires:  sed
BuildRequires:  diffutils
BuildRequires:  /usr/bin/makeinfo
Provides:      	bundled(gnulib)
Provides:      	bundled(libopts) = 41.1

%description
This is the set of GNU shar utilities.

shar makes shell archives out of many files, preparing them for
transmission by electronic mail services.  Use unshar to unpack shell
archives after reception.

uuencode prepares a file for transmission over an electronic channel
which ignores or otherwise mangles the eight bit (high order bit) of
bytes.	uudecode does the converse transformation.

remsync allows for remote synchronization of directory trees, using
e-mail.  This part of sharutils is still alpha.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

for i in TODO THANKS; do
  iconv -f iso-8859-1 -t utf-8 -o $i{.utf8,}
  mv $i{.utf8,}
done

%build
%configure
%make_build

%install
%make_install
rm -f %{buildroot}%{_infodir}/dir

chmod 644 AUTHORS ChangeLog COPYING NEWS README THANKS TODO

%find_lang %{name}

%check
%make_build check

%files -f %{name}.lang
%license COPYING AUTHORS
%doc README
%{_bindir}/*
%{_infodir}/*info*

%files help
%doc ChangeLog NEWS THANKS TODO
%{_mandir}/man?/*

%changelog
* Fri Oct 11 2024 Funda Wang <fundawang@yeah.net> - 4.15.2-17
- add buildrequires on makeinfo
- do not link md5.o twice into shar executable

* Sat Jul 31 2021 shixuantong <shixuantong@huawei.com> - 4.15.2-16
- Fix building with GCC 10

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.2-15
- Package init

* Thu Aug 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.15.2-14
- Package init
